﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Domen
{
    [Serializable]
    public class Posiljka
    {
        int id;
        string posiljalac;
        string primalac;
        string adresa;
        double masa;
        Mesto izMesto;
        Mesto uMesto;

        [Browsable(false)]
        public int Id { get => id; set => id = value; }
        public string Posiljalac { get => posiljalac; set => posiljalac = value; }
        public string Primalac { get => primalac; set => primalac = value; }
        
        public string Adresa { get => adresa; set => adresa = value; }
        public double Masa { get => masa; set => masa = value; }
        [Browsable(false)]
        public Mesto IzMesto { get => izMesto; set => izMesto = value; }
        [Browsable(false)]
        public Mesto UMesto { get => uMesto; set => uMesto = value; }
       
       

        public override string ToString()
        {
            return ("Sifra: " + id);
        }

    }
}

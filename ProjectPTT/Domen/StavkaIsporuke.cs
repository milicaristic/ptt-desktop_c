﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domen
{
    [Serializable]
   public class StavkaIsporuke
    {
        int rBr;
        bool hitno;
        int dnevnaIsporuka;
        Posiljka posiljka;

        public int RBr { get => rBr; set => rBr = value; }
        public bool Hitno { get => hitno; set => hitno = value; }
        public int DnevnaIsporuka { get => dnevnaIsporuka; set => dnevnaIsporuka = value; }
        public Posiljka Posiljka { get => posiljka; set => posiljka = value; }
    }
}

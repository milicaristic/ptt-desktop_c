﻿using Domen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Klijent
{
    public partial class FormaKlijent : Form
    {
        Komunikacija k;
        private Sluzbenik s;

        public FormaKlijent()
        {
            InitializeComponent();
        }

        public FormaKlijent(Sluzbenik s)
        {
            this.s = s;
            InitializeComponent();
            this.Text = "Dobro dosli: "+s.Ime+" "+s.Prezime+", Vreme prijave: "+DateTime.Now.ToString("hh:mm tt")+"";
        }

        private void FormaKlijent_Load(object sender, EventArgs e)
        {
            k = new Komunikacija();
            if (k.poveziSeNaServer())
            {
                this.Text = "Povezan";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            new UnosPosiljke().ShowDialog();
            this.Show();
        }

        private void btnPracenje_Click(object sender, EventArgs e)
        {
            this.Hide();
            new PretragaZaIzmenu().ShowDialog();
            this.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            new FiltrirajPoMestu().ShowDialog();
            this.Show();
            
        }
    }
}

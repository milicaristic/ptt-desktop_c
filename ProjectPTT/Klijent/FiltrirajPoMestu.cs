﻿using Domen;
using Server;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Klijent
{
    public partial class FiltrirajPoMestu : Form
    {
        public FiltrirajPoMestu()
        {
            InitializeComponent();
        }

        

        private void cmbIzMesta_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void FiltrirajPoMestu_Load(object sender, EventArgs e)
        {
            cmbIzMesta.DataSource = Broker.dajSesiju().vratiSvaMesta();
            cmbUMesto.DataSource = Broker.dajSesiju().vratiSvaMesta();

            



        }

        private void BtnIzMesta_Click(object sender, EventArgs e)
        {
            dataGridIzMesta.DataSource = Broker.dajSesiju().VratiSvePosiljkeIzMesta(cmbIzMesta.SelectedItem as Mesto);
            
        }

        private void btnUMesto_Click(object sender, EventArgs e)
        {
            dataGridUMesto.DataSource = Broker.dajSesiju().VratiSvePosiljkeIzMesta(cmbUMesto.SelectedItem as Mesto);
        }

        private void dataGridIzMesta_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}

﻿using Domen;
using Server;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Klijent
{
    public partial class Login : Form
    {
        int broj = 0;
        public Login()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            
           
            Sluzbenik s = new Sluzbenik();
            s.User = txtUsername.Text;
            s.Pass = txtPassword.Text;
            try
            {
                s = Broker.dajSesiju().login(s);
                if(s == null)
                {
                    broj++;
                    if (broj >= 3)
                    {
                        MessageBox.Show("Iskoristili ste 3 pokusaja");
                        txtUsername.ReadOnly = true;
                        txtUsername.BackColor = Color.DarkGray;
                        txtPassword.ReadOnly = true;
                        txtPassword.BackColor = Color.DarkGray;
                        btnLogin.Enabled = false;
                        return;
                    }
                    else
                    {
                        int preostaliBrPokusaja = 3 - broj;
                        MessageBox.Show("Greska! Preostalo Vam je " + preostaliBrPokusaja + ". pokusaja");
                    }
                    
                }
                else
                {
                    this.Hide();
                    new FormaKlijent(s).ShowDialog();
                    this.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void btnLogin_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
    }
}

﻿namespace Klijent
{
    partial class IzmeniPosiljku
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPosiljalac = new System.Windows.Forms.TextBox();
            this.txtPrimalac = new System.Windows.Forms.TextBox();
            this.txtAdresa = new System.Windows.Forms.TextBox();
            this.txtMasa = new System.Windows.Forms.TextBox();
            this.btnCuvanje = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbIzMesta = new System.Windows.Forms.ComboBox();
            this.cmbUMesto = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Posiljalac:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Primalac:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Adresa:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Masa:";
            // 
            // txtPosiljalac
            // 
            this.txtPosiljalac.Location = new System.Drawing.Point(119, 38);
            this.txtPosiljalac.Name = "txtPosiljalac";
            this.txtPosiljalac.Size = new System.Drawing.Size(193, 20);
            this.txtPosiljalac.TabIndex = 4;
            // 
            // txtPrimalac
            // 
            this.txtPrimalac.Location = new System.Drawing.Point(119, 76);
            this.txtPrimalac.Name = "txtPrimalac";
            this.txtPrimalac.Size = new System.Drawing.Size(193, 20);
            this.txtPrimalac.TabIndex = 5;
            // 
            // txtAdresa
            // 
            this.txtAdresa.Location = new System.Drawing.Point(119, 134);
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.Size = new System.Drawing.Size(193, 20);
            this.txtAdresa.TabIndex = 6;
            // 
            // txtMasa
            // 
            this.txtMasa.Location = new System.Drawing.Point(119, 168);
            this.txtMasa.Name = "txtMasa";
            this.txtMasa.Size = new System.Drawing.Size(193, 20);
            this.txtMasa.TabIndex = 7;
            // 
            // btnCuvanje
            // 
            this.btnCuvanje.Location = new System.Drawing.Point(119, 315);
            this.btnCuvanje.Name = "btnCuvanje";
            this.btnCuvanje.Size = new System.Drawing.Size(193, 38);
            this.btnCuvanje.TabIndex = 9;
            this.btnCuvanje.Text = "Sacuvaj izmene";
            this.btnCuvanje.UseVisualStyleBackColor = true;
            this.btnCuvanje.Click += new System.EventHandler(this.btnCuvanje_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 229);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Iz mesta:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 268);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "U mesto:";
            // 
            // cmbIzMesta
            // 
            this.cmbIzMesta.FormattingEnabled = true;
            this.cmbIzMesta.Location = new System.Drawing.Point(119, 220);
            this.cmbIzMesta.Name = "cmbIzMesta";
            this.cmbIzMesta.Size = new System.Drawing.Size(193, 21);
            this.cmbIzMesta.TabIndex = 12;
            // 
            // cmbUMesto
            // 
            this.cmbUMesto.FormattingEnabled = true;
            this.cmbUMesto.Location = new System.Drawing.Point(119, 260);
            this.cmbUMesto.Name = "cmbUMesto";
            this.cmbUMesto.Size = new System.Drawing.Size(193, 21);
            this.cmbUMesto.TabIndex = 13;
            // 
            // IzmeniPosiljku
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 365);
            this.Controls.Add(this.cmbUMesto);
            this.Controls.Add(this.cmbIzMesta);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCuvanje);
            this.Controls.Add(this.txtMasa);
            this.Controls.Add(this.txtAdresa);
            this.Controls.Add(this.txtPrimalac);
            this.Controls.Add(this.txtPosiljalac);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "IzmeniPosiljku";
            this.Text = "Izmena podataka";
            this.Load += new System.EventHandler(this.PodaciOPosiljci_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPosiljalac;
        private System.Windows.Forms.TextBox txtPrimalac;
        private System.Windows.Forms.TextBox txtAdresa;
        private System.Windows.Forms.TextBox txtMasa;
        private System.Windows.Forms.Button btnCuvanje;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbIzMesta;
        private System.Windows.Forms.ComboBox cmbUMesto;
    }
}
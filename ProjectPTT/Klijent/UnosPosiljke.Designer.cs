﻿using System;

namespace Klijent
{
    partial class UnosPosiljke
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbMestoU = new System.Windows.Forms.ComboBox();
            this.cmbMestoIz = new System.Windows.Forms.ComboBox();
            this.txtMasa = new System.Windows.Forms.TextBox();
            this.txtAdresa = new System.Windows.Forms.TextBox();
            this.txtPrimalac = new System.Windows.Forms.TextBox();
            this.txtPosiljalac = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnObrisiSve = new System.Windows.Forms.Button();
            this.btnObrisiJedan = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnSacuvajSve = new System.Windows.Forms.Button();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnDodaj);
            this.groupBox1.Controls.Add(this.cmbMestoU);
            this.groupBox1.Controls.Add(this.cmbMestoIz);
            this.groupBox1.Controls.Add(this.txtMasa);
            this.groupBox1.Controls.Add(this.txtAdresa);
            this.groupBox1.Controls.Add(this.txtPrimalac);
            this.groupBox1.Controls.Add(this.txtPosiljalac);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(506, 142);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Unos posiljke";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // cmbMestoU
            // 
            this.cmbMestoU.FormattingEnabled = true;
            this.cmbMestoU.Location = new System.Drawing.Point(302, 78);
            this.cmbMestoU.Name = "cmbMestoU";
            this.cmbMestoU.Size = new System.Drawing.Size(189, 21);
            this.cmbMestoU.TabIndex = 11;
            // 
            // cmbMestoIz
            // 
            this.cmbMestoIz.FormattingEnabled = true;
            this.cmbMestoIz.Location = new System.Drawing.Point(302, 53);
            this.cmbMestoIz.Name = "cmbMestoIz";
            this.cmbMestoIz.Size = new System.Drawing.Size(189, 21);
            this.cmbMestoIz.TabIndex = 10;
            // 
            // txtMasa
            // 
            this.txtMasa.Location = new System.Drawing.Point(63, 105);
            this.txtMasa.Name = "txtMasa";
            this.txtMasa.Size = new System.Drawing.Size(164, 20);
            this.txtMasa.TabIndex = 9;
            // 
            // txtAdresa
            // 
            this.txtAdresa.Location = new System.Drawing.Point(63, 79);
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.Size = new System.Drawing.Size(164, 20);
            this.txtAdresa.TabIndex = 8;
            // 
            // txtPrimalac
            // 
            this.txtPrimalac.Location = new System.Drawing.Point(63, 54);
            this.txtPrimalac.Name = "txtPrimalac";
            this.txtPrimalac.Size = new System.Drawing.Size(164, 20);
            this.txtPrimalac.TabIndex = 7;
            // 
            // txtPosiljalac
            // 
            this.txtPosiljalac.Location = new System.Drawing.Point(63, 29);
            this.txtPosiljalac.Name = "txtPosiljalac";
            this.txtPosiljalac.Size = new System.Drawing.Size(164, 20);
            this.txtPosiljalac.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(251, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Mesto u";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(250, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Mesto iz";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Masa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Adresa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Primalac";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Posiljalac";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnObrisiSve);
            this.groupBox2.Controls.Add(this.btnObrisiJedan);
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(12, 161);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(507, 193);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lista posiljki";
            // 
            // btnObrisiSve
            // 
            this.btnObrisiSve.Location = new System.Drawing.Point(303, 165);
            this.btnObrisiSve.Name = "btnObrisiSve";
            this.btnObrisiSve.Size = new System.Drawing.Size(189, 20);
            this.btnObrisiSve.TabIndex = 14;
            this.btnObrisiSve.Text = "Obrisi sve";
            this.btnObrisiSve.UseVisualStyleBackColor = true;
            this.btnObrisiSve.Click += new System.EventHandler(this.btnObrisiSve_Click);
            // 
            // btnObrisiJedan
            // 
            this.btnObrisiJedan.Location = new System.Drawing.Point(10, 165);
            this.btnObrisiJedan.Name = "btnObrisiJedan";
            this.btnObrisiJedan.Size = new System.Drawing.Size(189, 20);
            this.btnObrisiJedan.TabIndex = 13;
            this.btnObrisiJedan.Text = "Obrisi jedan";
            this.btnObrisiJedan.UseVisualStyleBackColor = true;
            this.btnObrisiJedan.Click += new System.EventHandler(this.btnObrisiJedan_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(10, 30);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(482, 131);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.AllowUserToAddRowsChanged += new System.EventHandler(this.UnosPosiljke_Load);
            this.dataGridView1.AutoSizeColumnsModeChanged += new System.Windows.Forms.DataGridViewAutoSizeColumnsModeEventHandler(this.fill);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // btnSacuvajSve
            // 
            this.btnSacuvajSve.Location = new System.Drawing.Point(176, 360);
            this.btnSacuvajSve.Name = "btnSacuvajSve";
            this.btnSacuvajSve.Size = new System.Drawing.Size(189, 31);
            this.btnSacuvajSve.TabIndex = 15;
            this.btnSacuvajSve.Text = "Sacuvaj sve";
            this.btnSacuvajSve.UseVisualStyleBackColor = true;
            this.btnSacuvajSve.Click += new System.EventHandler(this.btnSacuvajSve_Click);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(302, 104);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(189, 20);
            this.btnDodaj.TabIndex = 15;
            this.btnDodaj.Text = "Dodaj u listu";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // UnosPosiljke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 402);
            this.Controls.Add(this.btnSacuvajSve);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "UnosPosiljke";
            this.Text = "UnosPosiljke";
            this.Load += new System.EventHandler(this.UnosPosiljke_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        private void btnSacuvajPosiljku_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbMestoU;
        private System.Windows.Forms.ComboBox cmbMestoIz;
        private System.Windows.Forms.TextBox txtMasa;
        private System.Windows.Forms.TextBox txtAdresa;
        private System.Windows.Forms.TextBox txtPrimalac;
        private System.Windows.Forms.TextBox txtPosiljalac;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnObrisiSve;
        private System.Windows.Forms.Button btnObrisiJedan;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnSacuvajSve;
        private System.Windows.Forms.Button btnDodaj;
    }
}
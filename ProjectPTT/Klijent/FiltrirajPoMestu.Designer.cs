﻿namespace Klijent
{
    partial class FiltrirajPoMestu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmbIzMesta = new System.Windows.Forms.ComboBox();
            this.cmbUMesto = new System.Windows.Forms.ComboBox();
            this.dataGridUMesto = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridIzMesta = new System.Windows.Forms.DataGridView();
            this.BtnIzMesta = new System.Windows.Forms.Button();
            this.btnUMesto = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridUMesto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridIzMesta)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Posiljke iz mesta:";
            // 
            // cmbIzMesta
            // 
            this.cmbIzMesta.FormattingEnabled = true;
            this.cmbIzMesta.Location = new System.Drawing.Point(105, 21);
            this.cmbIzMesta.Name = "cmbIzMesta";
            this.cmbIzMesta.Size = new System.Drawing.Size(163, 21);
            this.cmbIzMesta.TabIndex = 3;
            this.cmbIzMesta.SelectedIndexChanged += new System.EventHandler(this.cmbIzMesta_SelectedIndexChanged);
            // 
            // cmbUMesto
            // 
            this.cmbUMesto.FormattingEnabled = true;
            this.cmbUMesto.Location = new System.Drawing.Point(105, 166);
            this.cmbUMesto.Name = "cmbUMesto";
            this.cmbUMesto.Size = new System.Drawing.Size(163, 21);
            this.cmbUMesto.TabIndex = 6;
            // 
            // dataGridUMesto
            // 
            this.dataGridUMesto.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridUMesto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridUMesto.Location = new System.Drawing.Point(12, 193);
            this.dataGridUMesto.Name = "dataGridUMesto";
            this.dataGridUMesto.Size = new System.Drawing.Size(511, 106);
            this.dataGridUMesto.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Posiljke u mesto:";
            // 
            // dataGridIzMesta
            // 
            this.dataGridIzMesta.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridIzMesta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridIzMesta.Location = new System.Drawing.Point(12, 48);
            this.dataGridIzMesta.Name = "dataGridIzMesta";
            this.dataGridIzMesta.Size = new System.Drawing.Size(511, 106);
            this.dataGridIzMesta.TabIndex = 7;
            this.dataGridIzMesta.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridIzMesta_CellContentClick);
            // 
            // BtnIzMesta
            // 
            this.BtnIzMesta.Location = new System.Drawing.Point(395, 19);
            this.BtnIzMesta.Name = "BtnIzMesta";
            this.BtnIzMesta.Size = new System.Drawing.Size(128, 23);
            this.BtnIzMesta.TabIndex = 8;
            this.BtnIzMesta.Text = "Pretrazi";
            this.BtnIzMesta.UseVisualStyleBackColor = true;
            this.BtnIzMesta.Click += new System.EventHandler(this.BtnIzMesta_Click);
            // 
            // btnUMesto
            // 
            this.btnUMesto.Location = new System.Drawing.Point(395, 164);
            this.btnUMesto.Name = "btnUMesto";
            this.btnUMesto.Size = new System.Drawing.Size(128, 23);
            this.btnUMesto.TabIndex = 9;
            this.btnUMesto.Text = "Pretrazi";
            this.btnUMesto.UseVisualStyleBackColor = true;
            this.btnUMesto.Click += new System.EventHandler(this.btnUMesto_Click);
            // 
            // FiltrirajPoMestu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 319);
            this.Controls.Add(this.btnUMesto);
            this.Controls.Add(this.BtnIzMesta);
            this.Controls.Add(this.dataGridIzMesta);
            this.Controls.Add(this.cmbUMesto);
            this.Controls.Add(this.dataGridUMesto);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbIzMesta);
            this.Controls.Add(this.label1);
            this.Name = "FiltrirajPoMestu";
            this.Text = "Filtriraj posiljke po mestu";
            this.Load += new System.EventHandler(this.FiltrirajPoMestu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridUMesto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridIzMesta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbIzMesta;
        private System.Windows.Forms.ComboBox cmbUMesto;
        private System.Windows.Forms.DataGridView dataGridUMesto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridIzMesta;
        private System.Windows.Forms.Button BtnIzMesta;
        private System.Windows.Forms.Button btnUMesto;
    }
}
﻿using Domen;
using Server;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Klijent
{
    public partial class IzmeniPosiljku : Form
    {
        private Posiljka p;
        Komunikacija k;
    
        public IzmeniPosiljku()
        {
           
        }

        public IzmeniPosiljku(Posiljka p, Komunikacija k)
        {
            InitializeComponent();
            this.p = p;
            this.k = k;
            
        }

        private void PodaciOPosiljci_Load(object sender, EventArgs e)
        {
            cmbIzMesta.DataSource = Broker.dajSesiju().vratiSvaMesta();
            cmbUMesto.DataSource = Broker.dajSesiju().vratiSvaMesta();

            txtPosiljalac.Text = p.Posiljalac.ToString();
            txtPrimalac.Text = p.Primalac.ToString();
            txtMasa.Text = Convert.ToString(p.Masa);
            txtAdresa.Text = p.Adresa.ToString();

            cmbIzMesta.Text = "Unesite mesto odakle se salje";
            cmbUMesto.Text = "Unesite mesto gde se salje";

        }

        private void btnCuvanje_Click(object sender, EventArgs e)
        {
            Posiljka po = new Posiljka();


            po.Posiljalac = txtPosiljalac.Text;
            if (po.Posiljalac == "")
            {
                MessageBox.Show("Niste uneli/izmenili posiljaoca");
                txtPosiljalac.Focus();
                return;

            }

            po.Primalac = txtPrimalac.Text;
            if (po.Primalac == "")
            {
                MessageBox.Show("Niste uneli/izmenili primaoca");
                txtPrimalac.Focus();
                return;

            }
            po.Adresa = txtAdresa.Text;
            if (po.Adresa == "")
            {
                MessageBox.Show("Niste uneli/izmenili adresu");
                txtAdresa.Focus();
                return;

            }

            try
            {
                po.Masa = Convert.ToDouble(txtMasa.Text);

            }
            catch (Exception)
            {
                if (po.Masa <= 0)
                {
                    MessageBox.Show("Masa nije u odgovarajucem formatu");
                    txtMasa.Focus();
                    return;
                }
            }
            po.IzMesto = cmbIzMesta.SelectedItem as Mesto;
            if (po.IzMesto == null)
            {
                MessageBox.Show("Niste izabrali mesto odakle se posiljka salje");
                return;
            }
            po.UMesto = cmbUMesto.SelectedItem as Mesto;
            if (po.UMesto == null)
            {
                MessageBox.Show("Niste izabrali mesto u koje se posiljka salje");
                return;
            }

            p.Posiljalac = po.Posiljalac;
            p.Primalac = po.Primalac;
            p.Adresa = po.Adresa;
            p.Masa = po.Masa;
            p.IzMesto = po.IzMesto;
            p.UMesto = po.UMesto;

            try
            {
                int rez = Broker.dajSesiju().izmeniPosiljku(p);
                if (rez == 0)
                {
                    MessageBox.Show("Neuspesna izmena podataka o posiljci");
                }
                else
                {
                    MessageBox.Show("Uspesno ste izmenili podatke o posiljci!");
                    this.Close();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            

          


        }

    }
    }


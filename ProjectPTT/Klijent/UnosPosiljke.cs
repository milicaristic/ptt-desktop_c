﻿using Domen;
using Server;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Klijent
{
    public partial class UnosPosiljke : Form
    {

        BindingList<Posiljka> listaPosiljki;
        public UnosPosiljke()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void UnosPosiljke_Load(object sender, EventArgs e)
        {
            listaPosiljki = new BindingList<Posiljka>();
            dataGridView1.DataSource = listaPosiljki;

            cmbMestoIz.DataSource = Broker.dajSesiju().vratiSvaMesta();
            cmbMestoU.DataSource = Broker.dajSesiju().vratiSvaMesta();

            cmbMestoIz.Text = "Odaberite mesto iz kog saljete";
            cmbMestoU.Text = "Odaberite mesto u koje saljete";

        }

        
        private void fill(object sender, DataGridViewAutoSizeColumnsModeEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnObrisiJedan_Click(object sender, EventArgs e)
        {
            try
            {
                Posiljka p = dataGridView1.CurrentRow.DataBoundItem as Posiljka;
                listaPosiljki.Remove(p);
            }
            catch (Exception)
            {

                MessageBox.Show("Niste odabrali posiljku koju zelite da obrisete");
            }
        }

        private void btnObrisiSve_Click(object sender, EventArgs e)
        {
            listaPosiljki.Clear();
        }


        private void btnSacuvajSve_Click(object sender, EventArgs e)
        {
            try
            {
                int rez = Broker.dajSesiju().sacuvajPosiljke(new List<Posiljka>(listaPosiljki));
                if(rez == 0)
                {
                    MessageBox.Show("Posiljke nisu uspesno sacuvane");
                }
                else
                {
                    MessageBox.Show("Posiljke su uspesno sacuvane"); 
                    listaPosiljki.Clear();
                    this.Hide();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            Posiljka p = new Posiljka();

            p.Id = Broker.dajSesiju().vratiSifruPosiljke();

            p.Posiljalac = txtPosiljalac.Text;
            if (p.Posiljalac == "")
            {
                MessageBox.Show("Niste uneli posiljaoca");
                txtPosiljalac.Focus();
                return;

            }

            p.Primalac = txtPrimalac.Text;
            if (p.Primalac == "")
            {
                MessageBox.Show("Niste uneli primaoca");
                txtPrimalac.Focus();
                return;

            }
            p.Adresa = txtAdresa.Text;
            if (p.Adresa == "")
            {
                MessageBox.Show("Niste uneli adresu");
                txtAdresa.Focus();
                return;

            }

            try
            {
                p.Masa = Convert.ToDouble(txtMasa.Text);

            }
            catch (Exception)
            {
                if (p.Masa <= 0)
                {
                    MessageBox.Show("Masa nije u odgovarajucem formatu");
                    txtMasa.Focus();
                    return;
                }
            }
            p.IzMesto = cmbMestoIz.SelectedItem as Mesto;
            if (p.IzMesto == null)
            {
                MessageBox.Show("Niste izabrali mesto odakle se posiljka salje");
                return;
            }
            p.UMesto = cmbMestoU.SelectedItem as Mesto;
            if (p.UMesto == null)
            {
                MessageBox.Show("Niste izabrali mesto u koje se posiljka salje");
                return;
            }


            listaPosiljki.Add(p);
            txtAdresa.Clear();
            txtMasa.Clear();
            txtPrimalac.Clear();
            
            cmbMestoU.Text = "Odaberite mesto u koje saljete";

        }
    }
}

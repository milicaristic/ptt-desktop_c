﻿using Domen;
using Server;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Klijent
{
    public partial class PretragaZaIzmenu : Form
    {
        Komunikacija k;
        
        public PretragaZaIzmenu()
        {
            
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Posiljka p = new Posiljka();
           
            
            try
            {
              
                int id = Convert.ToInt32(txtSifraP.Text);
                p = Broker.dajSesiju().vratiOdgPosiljku(id);
               
                if(p == null)
                {
                    MessageBox.Show("Ne postoji posiljka sa unetim id-jem");
                }
                else
                {
                    new IzmeniPosiljku(p,k).ShowDialog();
                    this.Hide();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Niste uneli sifru");
            }
  
        }

        private void PracenjePosiljke_Load(object sender, EventArgs e)
        {
            k = new Komunikacija();
            if (k.poveziSeNaServer())
            {
                this.Text = "Izmena podataka o posiljci";
            }
        }
    }
}

﻿namespace Klijent
{
    partial class PretragaZaIzmenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPrikaziPodatke = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSifraP = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnPrikaziPodatke
            // 
            this.btnPrikaziPodatke.Location = new System.Drawing.Point(286, 83);
            this.btnPrikaziPodatke.Name = "btnPrikaziPodatke";
            this.btnPrikaziPodatke.Size = new System.Drawing.Size(137, 42);
            this.btnPrikaziPodatke.TabIndex = 0;
            this.btnPrikaziPodatke.Text = "Prikazi podatke";
            this.btnPrikaziPodatke.UseVisualStyleBackColor = true;
            this.btnPrikaziPodatke.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(267, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Unesite sifru posiljke za koju zelite da dobijete podatke:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtSifraP
            // 
            this.txtSifraP.Location = new System.Drawing.Point(285, 41);
            this.txtSifraP.Name = "txtSifraP";
            this.txtSifraP.Size = new System.Drawing.Size(137, 20);
            this.txtSifraP.TabIndex = 2;
            // 
            // PretragaZaIzmenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 145);
            this.Controls.Add(this.txtSifraP);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnPrikaziPodatke);
            this.Name = "PretragaZaIzmenu";
            this.Text = "Izmena";
            this.Load += new System.EventHandler(this.PracenjePosiljke_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPrikaziPodatke;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSifraP;
    }
}
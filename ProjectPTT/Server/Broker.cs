﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domen;
using System.Data;
using System.Data.SqlClient;

namespace Server
{
    public class Broker
    {
        SqlCommand komanda;
        SqlConnection konekcija;
        SqlTransaction transakcija;

        void konektujSe()
        {
            konekcija = new SqlConnection(@"Data Source=DESKTOP-UC066MS\SQLEXPRESS;Initial Catalog=KurirskaSluzba;Integrated Security=True");
            komanda = konekcija.CreateCommand();
        }

        //Singlton - omogucava stalnu konekciju nad bazom i da bude uvek 1 aktivna konekcija

        Broker()
        {
            konektujSe();
        }

        static Broker instanca;
        public static Broker dajSesiju()
        {
            if (instanca == null) instanca = new Broker();
            return instanca;
        }

        

        public Sluzbenik login(Sluzbenik s)
        {
            try
            {
                konekcija.Open();
                komanda.CommandText = "select * from Sluzbenik where [User] = '" + s.User + "' and Pass = '" + s.Pass + "'";
                SqlDataReader citac = komanda.ExecuteReader();
                if (citac.Read())
                {
                    s.Id = citac.GetInt32(0);
                    s.Ime = citac.GetString(1);
                    s.Prezime = citac.GetString(2);
                    return s;

                }
                citac.Close();
                return null;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (konekcija != null) konekcija.Close();
            }
        }

        public List<Mesto> vratiSvaMesta()
        {
            List<Mesto> lista = new List<Mesto>();
            try
            {
                konekcija.Open();
                komanda.CommandText = "Select * from Mesto";
                SqlDataReader citac = komanda.ExecuteReader();
                while (citac.Read())
                {
                    Mesto m = new Mesto();
                    m.Id = citac.GetInt32(0);
                    m.Ptt = citac.GetString(1);
                    m.Naziv = citac.GetString(2);
                    lista.Add(m);
                }
                citac.Close();
                return lista;

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (konekcija != null) konekcija.Close();
            }
        }

        public int vratiSifruPosiljke()
        {
           
            try
            {
                komanda.CommandText = "select max(id) from posiljka";
                try
                {
                    int sifra = Convert.ToInt32(komanda.ExecuteScalar());
                    return sifra + 1;
                }
                catch (Exception)
                {
                    return 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
            
        public int sacuvajPosiljku(Posiljka p)
        {
            try
            {
                konekcija.Open();
                p.Id = vratiSifruPosiljke();
                komanda.CommandText = "insert into posiljka values(" + p.Id + ", '" + p.Posiljalac + "', '" + p.Primalac + "', '" + p.Adresa + "', " + p.Masa + ", " + p.IzMesto.Id + ", " + p.UMesto.Id + ")";
                return komanda.ExecuteNonQuery();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (konekcija != null) konekcija.Close();
            }
        }

        public int sacuvajPosiljke(List<Posiljka> lista)
        {
            try
            {
                konekcija.Open();
                transakcija = konekcija.BeginTransaction();
                komanda = new SqlCommand("", konekcija, transakcija);
                foreach (Posiljka p in lista)
                {
                    p.Id = vratiSifruPosiljke();
                    komanda.CommandText = "insert into posiljka values(" + p.Id + ", '" + p.Posiljalac + "', '" + p.Primalac + "', '" + p.Adresa + "', " + p.Masa + ", " + p.IzMesto.Id + ", " + p.UMesto.Id + ")";
                    komanda.ExecuteNonQuery();

                }
                transakcija.Commit();
                return 1;

            }
            catch (Exception)
            {
                transakcija.Rollback();
                throw;
            }
            finally
            {
                if (konekcija != null) konekcija.Close();
            }
        }

        public Posiljka vratiOdgPosiljku(int id)
        {
            Posiljka p = new Posiljka();
            try
            {
                konekcija.Open();
                komanda.CommandText = "Select * from posiljka where id = " + id ;
                SqlDataReader citac = komanda.ExecuteReader();
                if(citac.Read())
                {
                    
                    p.Id = citac.GetInt32(0);
                    p.Posiljalac = citac.GetString(1);
                    p.Primalac = citac.GetString(2);
                    p.Adresa = citac.GetString(3);
                   
                    try
                    {
                        p.Masa = Convert.ToDouble(citac.GetValue(4));
                    }
                    catch (Exception)
                    {

                    }
                    p.IzMesto = new Mesto();
                    p.IzMesto.Id = citac.GetInt32(5);
                    p.UMesto = new Mesto();
                    p.UMesto.Id = citac.GetInt32(6);
                    return p;
                }

                citac.Close();
                return null;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (konekcija != null) konekcija.Close();
            }
        }

        public int izmeniPosiljku(Posiljka p)
        {
            try
            {
                konekcija.Open();
                komanda.CommandText = "Update Posiljka set posiljalac= '" + p.Posiljalac + "', primalac ='" + p.Primalac + "', adresa= '" + p.Adresa + "', masa = " + p.Masa + ", iZmestoid =" + p.IzMesto.Id + ", uMestoid =" + p.UMesto.Id + " where id = " + p.Id + " ";

                int rezultat = komanda.ExecuteNonQuery();
                return rezultat;
               


            }
            catch (Exception)
            {
                
                throw;
            }
            finally
            {
                if (konekcija != null) konekcija.Close();
            }
        }

        public List<Posiljka> VratiSvePosiljkeIzMesta(Mesto mesto)
        {
            List<Posiljka> listaPosiljki = new List<Posiljka>();
            try
            {
                konekcija.Open();
                komanda.CommandText = "Select * from Posiljka where IzMestoID= "+mesto.Id+"";
                SqlDataReader citac = komanda.ExecuteReader();
                while (citac.Read())
                {
                    Posiljka p = new Posiljka();
                    p.Id = citac.GetInt32(0);
                    p.Posiljalac = citac.GetString(1);
                    p.Primalac = citac.GetString(2);
                    p.Adresa = citac.GetString(3);
                    try
                    {
                        p.Masa = Convert.ToDouble(citac.GetValue(4));
                    }
                    catch (Exception)
                    {

                    }
                    listaPosiljki.Add(p);

                }
                citac.Close();
                return listaPosiljki;
               
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (konekcija != null) konekcija.Close();
            }

        }
    }
}

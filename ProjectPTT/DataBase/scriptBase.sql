USE [master]
GO
/****** Object:  Database [KurirskaSluzba]    Script Date: 5/8/2021 8:11:40 PM ******/
CREATE DATABASE [KurirskaSluzba]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'KurirskaSluzba', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\KurirskaSluzba.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'KurirskaSluzba_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\KurirskaSluzba_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [KurirskaSluzba] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [KurirskaSluzba].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [KurirskaSluzba] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET ARITHABORT OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [KurirskaSluzba] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [KurirskaSluzba] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET  DISABLE_BROKER 
GO
ALTER DATABASE [KurirskaSluzba] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [KurirskaSluzba] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [KurirskaSluzba] SET  MULTI_USER 
GO
ALTER DATABASE [KurirskaSluzba] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [KurirskaSluzba] SET DB_CHAINING OFF 
GO
ALTER DATABASE [KurirskaSluzba] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [KurirskaSluzba] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [KurirskaSluzba] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [KurirskaSluzba] SET QUERY_STORE = OFF
GO
USE [KurirskaSluzba]
GO
/****** Object:  Table [dbo].[DnevnaIsporuka]    Script Date: 5/8/2021 8:11:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DnevnaIsporuka](
	[ID] [int] NOT NULL,
	[Datum] [date] NULL,
	[KurirID] [int] NULL,
	[SluzbenikID] [int] NULL,
 CONSTRAINT [PK_DnevnaIsporuka] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Kurir]    Script Date: 5/8/2021 8:11:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kurir](
	[ID] [int] NOT NULL,
	[Ime] [varchar](50) NULL,
	[Prezime] [varchar](50) NULL,
 CONSTRAINT [PK_Kurir] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mesto]    Script Date: 5/8/2021 8:11:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mesto](
	[ID] [int] NOT NULL,
	[PTT] [varchar](50) NULL,
	[Naziv] [varchar](50) NULL,
 CONSTRAINT [PK_Mesto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Posiljka]    Script Date: 5/8/2021 8:11:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Posiljka](
	[ID] [int] NOT NULL,
	[Posiljalac] [varchar](50) NULL,
	[Primalac] [varchar](50) NULL,
	[Adresa] [varchar](50) NULL,
	[Masa] [numeric](18, 2) NULL,
	[IzMestoID] [int] NULL,
	[UMestoID] [int] NULL,
 CONSTRAINT [PK_Posiljka] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sluzbenik]    Script Date: 5/8/2021 8:11:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sluzbenik](
	[ID] [int] NOT NULL,
	[Ime] [varchar](50) NULL,
	[Prezime] [varchar](50) NULL,
	[User] [varchar](50) NULL,
	[Pass] [varchar](50) NULL,
 CONSTRAINT [PK_Sluzbenik] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StavkaIsporuke]    Script Date: 5/8/2021 8:11:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StavkaIsporuke](
	[IsporukaID] [int] NOT NULL,
	[RBr] [int] NOT NULL,
	[Hitno] [bit] NULL,
	[PosiljkaID] [int] NULL,
 CONSTRAINT [PK_StavkaIsporuke] PRIMARY KEY CLUSTERED 
(
	[IsporukaID] ASC,
	[RBr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Kurir] ([ID], [Ime], [Prezime]) VALUES (1, N'Petar', N'Jovic')
INSERT [dbo].[Kurir] ([ID], [Ime], [Prezime]) VALUES (2, N'Milena', N'Mikic')
INSERT [dbo].[Kurir] ([ID], [Ime], [Prezime]) VALUES (3, N'Svetlana', N'Pejcic')
INSERT [dbo].[Kurir] ([ID], [Ime], [Prezime]) VALUES (4, N'Ivan', N'Ristic')
INSERT [dbo].[Kurir] ([ID], [Ime], [Prezime]) VALUES (5, N'Ana', N'Ivanovic')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (1, N'11000', N'Beograd')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (2, N'21000', N'Novi Sad')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (3, N'34000', N'Kragujevac')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (4, N'18000', N'Nis')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (5, N'37000', N'Krusevac')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (6, N'24000', N'Subotica')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (7, N'31000', N'Uzice')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (8, N'23000', N'Zrenjanin')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (9, N'36000', N'Kraljevo')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (10, N'11300', N'Smederevo')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (11, N'32000', N'Cacak')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (12, N'36300', N'Novi Pazar')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (13, N'15000', N'Sabac')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (14, N'11500', N'Obrenovac')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (15, N'18300', N'Pirot')
INSERT [dbo].[Mesto] ([ID], [PTT], [Naziv]) VALUES (16, N'14000', N'Valjevo')
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (1, N'Marko', N'Ivan', N'Koste Glavinica 20', CAST(33.00 AS Numeric(18, 2)), 6, 3)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (2, N'Jovan', N'Milica', N'Ivanova 42', CAST(1111.00 AS Numeric(18, 2)), 7, 4)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (3, N'Sanja', N'Sara', N'Perina 20', CAST(1000.00 AS Numeric(18, 2)), 8, 5)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (4, N'Nikola', N'Milica', N'Pere Todorovica 22', CAST(100.00 AS Numeric(18, 2)), 9, 6)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (5, N'Ostoja', N'Milica', N'Njegoseva 21b', CAST(1212444.00 AS Numeric(18, 2)), 10, 7)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (6, N'Kosta', N'Jovana', N'Sarajevska 34', CAST(433.00 AS Numeric(18, 2)), 13, 2)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (7, N'Ivana', N'Sofija', N'Borisavljeviceva 2a', CAST(4334.00 AS Numeric(18, 2)), 14, 1)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (8, N'Jelena', N'Emilija', N'Jove Ilica 234', CAST(222.00 AS Numeric(18, 2)), 15, 8)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (9, N'Katarina', N'Luka', N'Vojvode Stepe 22', CAST(3333.00 AS Numeric(18, 2)), 16, 9)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (10, N'Slavica', N'Filip', N'Pozeska 66', CAST(9090.00 AS Numeric(18, 2)), 11, 10)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (11, N'Tamara', N'Relja', N'Gandijeva 100', CAST(233.00 AS Numeric(18, 2)), 12, 11)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (12, N'Dusan', N'Tadija', N'Kneza Viseslava 10', CAST(222.00 AS Numeric(18, 2)), 5, 12)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (13, N'Pedja', N'Iman', N'Resavska 12a', CAST(102.00 AS Numeric(18, 2)), 4, 13)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (14, N'Aleksandar', N'Neda', N'Kijevska 21', CAST(1233.00 AS Numeric(18, 2)), 3, 14)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (15, N'Dragan', N'Jelena', N'Pere Velimirovica 8', CAST(45.00 AS Numeric(18, 2)), 2, 15)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (16, N'Milica', N'Jovana', N'Prvomajska 29', CAST(90.00 AS Numeric(18, 2)), 1, 16)
INSERT [dbo].[Posiljka] ([ID], [Posiljalac], [Primalac], [Adresa], [Masa], [IzMestoID], [UMestoID]) VALUES (17, N'Novica', N'Nemanja', N'Nemanjina 3', CAST(23.00 AS Numeric(18, 2)), 13, 5)
INSERT [dbo].[Sluzbenik] ([ID], [Ime], [Prezime], [User], [Pass]) VALUES (1, N'Ivan', N'Pejcic', N'ivan', N'ivan123')
INSERT [dbo].[Sluzbenik] ([ID], [Ime], [Prezime], [User], [Pass]) VALUES (2, N'Petar', N'Ivanovic', N'petar', N'pera123')
INSERT [dbo].[Sluzbenik] ([ID], [Ime], [Prezime], [User], [Pass]) VALUES (3, N'Milica', N'Stojakovic', N'milica', N'mica123')
INSERT [dbo].[Sluzbenik] ([ID], [Ime], [Prezime], [User], [Pass]) VALUES (4, N'Sofija', N'Ilic', N'sofija', N'sofi123')
INSERT [dbo].[Sluzbenik] ([ID], [Ime], [Prezime], [User], [Pass]) VALUES (5, N'Filip', N'Aleksic', N'filip', N'fica123')
ALTER TABLE [dbo].[DnevnaIsporuka]  WITH CHECK ADD  CONSTRAINT [FK_DnevnaIsporuka_Kurir] FOREIGN KEY([KurirID])
REFERENCES [dbo].[Kurir] ([ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DnevnaIsporuka] CHECK CONSTRAINT [FK_DnevnaIsporuka_Kurir]
GO
ALTER TABLE [dbo].[DnevnaIsporuka]  WITH CHECK ADD  CONSTRAINT [FK_DnevnaIsporuka_Sluzbenik] FOREIGN KEY([SluzbenikID])
REFERENCES [dbo].[Sluzbenik] ([ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DnevnaIsporuka] CHECK CONSTRAINT [FK_DnevnaIsporuka_Sluzbenik]
GO
ALTER TABLE [dbo].[Posiljka]  WITH CHECK ADD  CONSTRAINT [FK_Posiljka_Mesto] FOREIGN KEY([IzMestoID])
REFERENCES [dbo].[Mesto] ([ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Posiljka] CHECK CONSTRAINT [FK_Posiljka_Mesto]
GO
ALTER TABLE [dbo].[Posiljka]  WITH CHECK ADD  CONSTRAINT [FK_Posiljka_Mesto1] FOREIGN KEY([UMestoID])
REFERENCES [dbo].[Mesto] ([ID])
GO
ALTER TABLE [dbo].[Posiljka] CHECK CONSTRAINT [FK_Posiljka_Mesto1]
GO
ALTER TABLE [dbo].[StavkaIsporuke]  WITH CHECK ADD  CONSTRAINT [FK_StavkaIsporuke_DnevnaIsporuka] FOREIGN KEY([IsporukaID])
REFERENCES [dbo].[DnevnaIsporuka] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[StavkaIsporuke] CHECK CONSTRAINT [FK_StavkaIsporuke_DnevnaIsporuka]
GO
ALTER TABLE [dbo].[StavkaIsporuke]  WITH CHECK ADD  CONSTRAINT [FK_StavkaIsporuke_Posiljka] FOREIGN KEY([PosiljkaID])
REFERENCES [dbo].[Posiljka] ([ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[StavkaIsporuke] CHECK CONSTRAINT [FK_StavkaIsporuke_Posiljka]
GO
USE [master]
GO
ALTER DATABASE [KurirskaSluzba] SET  READ_WRITE 
GO
